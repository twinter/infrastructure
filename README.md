# Infrastructure

This is the documentation and required files to set up and configure my own infrastructure.

!!! Please note that this is highly specific to my own systems and will not be transferable to other systems. I do hope
that it might be useful as inspiration or a guide though.

## Requirements

### SSH key

My SSH key has to be installed on all machines.
One of the easier ways is to use `ssh-copy-id`. A simple example: `ssh-copy-id -fi public_key.pub root@192.168.0.101`.

The SSH key has to be available to the system either via the default location or an agent.

### Dependencies

You need to have at least the following installed:
- ansible
- terraform

## Deployment

Run `ansible-playbook -i inventory.yml playbook.yml`

## IP Ranges

| CIDR block       | Users                            |
|:-----------------|:---------------------------------|
| 192.168.0.0/24   | misc (router, physical nodes, …) |
| 192.168.1.0/24   | virtual nodes (mostly k8s nodes) |
| 192.168.128.0/17 | Kubernetes pods                  |
