#!/usr/bin/env bash

set -euo pipefail

PUSHD_FINISHED=0

exit_trap () {
  local lc="$BASH_COMMAND" rc=$?
  echo "Command [$lc] exited with code [$rc]"
  if [ $PUSHD_FINISHED = 1 ]; then
      popd > /dev/null || retun
  fi
}
trap exit_trap EXIT

pushd "$(dirname -- "${BASH_SOURCE[0]}")" > /dev/null || exit 1
PUSHD_FINISHED=1

ansible-lint
ansible-playbook -i inventory.yml playbook.yml
